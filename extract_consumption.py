#!/usr/bin/env python3

# Copyright (c) 2022 Inria
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import pathlib
import re
import os
import json
import math

def extract_consumption(input_file: pathlib.Path):
    with open(input_file, encoding='utf-8') as f:
        uid = None
        for line in f.readlines():
            ma = re.match(r'.*(u[a-z0-9]+):"com\.android\.chrome"', line)
            if ma:
                uid = ma.groups()[0]

            if uid:
                ma = re.compile(f'.*Uid {uid}: ([0-9.]+) \( cpu=([0-9.]+) wifi=([0-9.]+) \)')\
                    .match(line)
                if ma:
                    print(input_file, ma.groups())
                    return ma.groups()

    return math.nan


def extract_data(input_dir: pathlib.Path, output_json: pathlib.Path):
    devices = set([re.match(r'batterystats_[^_]*_([^_]+)', f).groups()[0]
                   for f in os.listdir(input_dir)])

    data = {}

    for device in devices:
        plain = [f for f in os.listdir(input_dir)
                 if re.compile(f'batterystats_0_{device}.*:.*:8\d\d0.*\.txt').match(f)]
        nobootstrapjs = [f for f in os.listdir(input_dir)
                         if re.compile(f'batterystats_0_{device}.*:.*:8\d\d1.*\.txt').match(f)]
        jsrehab = [f for f in os.listdir(input_dir)
                   if re.compile(f'batterystats_0_{device}.*:.*:8\d\d2.*\.txt').match(f)]

        data[device] = {
            'plain': [
                {
                    'file': f,
                    'url': re.sub(r'^(.*:8\d\d)\d(.*)_\d+\.txt', r'\1\2', f),
                    'energy': extract_consumption(input_dir.joinpath(f)),
                }
                for f in plain
            ],
            'nobootstrapjs': [
                {
                    'file': f,
                    'url': re.sub(r'^(.*:8\d\d)\d(.*)_\d+\.txt', r'\1\2', f),
                    'energy': extract_consumption(input_dir.joinpath(f)),
                }
                for f in nobootstrapjs
            ],
            'jsrehab': [
                {
                    'file': f,
                    'url': re.sub(r'^(.*:8\d\d)\d(.*)_\d+\.txt', r'\1\2', f),
                    'energy': extract_consumption(input_dir.joinpath(f)),
                }
                for f in jsrehab
            ],
        }


    print(json.dumps(data, indent=2))
    with open(output_json, 'w', encoding='utf-8') as of:
        json.dump(data, of)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", type=pathlib.Path, metavar="INPUT",
                        help="input directory")
    parser.add_argument("-o", type=pathlib.Path, required=True,
                        dest='output_json', help="output JSON")
    args = parser.parse_args()

    extract_data(args.input_dir, args.output_json)
