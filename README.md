# energy-consumption-measurements
This repository contains the scripts used to measure the energy consumption of
browsers running on Android phones, by collecting the battery statistics reported
by `adb`.

## License
Licensed under the MIT license (see LICENSE.txt)

Copyright (c) 2022 Inria
