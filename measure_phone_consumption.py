#!/usr/bin/env python3

# Copyright (c) 2022 Inria
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import argparse
import pathlib
import subprocess
import time


def bypass_chrome_https_warning(
    adb_device: str,
    screen_width: int,
    screen_height: int,
    browser: str,
) -> None:
    print("Bypassing the Chrome HTTPS warning page")

    pos_ratios = {
        'advanced': {
            '50': {'x': 0.5, 'y': 0.859},
            '96': {'x': 0.5, 'y': 0.859},
        },
        'unsafe': {
            '50': {'x': 0.292, 'y': 0.562},
            '96': {'x': 0.292, 'y': 0.469},
        },
    }

    version = '50' if browser == 'oldchrome' else '96'

    subprocess.run(
        [
            'adb',
            '-s', adb_device,
            'shell', 'input', 'tap',
            str(int(pos_ratios['advanced'][version]['x'] * screen_width)),
            str(int(pos_ratios['advanced'][version]['y'] * screen_height)),
        ],
        check=True,
    )
    time.sleep(0.5)
    subprocess.run(
        [
            'adb',
            '-s', adb_device,
            'shell', 'input', 'tap',
            str(int(pos_ratios['unsafe'][version]['x'] * screen_width)),
            str(int(pos_ratios['unsafe'][version]['y'] * screen_height)),
        ],
        check=True,
    )


def normalize_url(url: str) -> str:
    return url.replace('/', '_')


def measure_power_consumption(
    url_file: pathlib.Path,
    adb_device: str,
    device_name: str,
    browser: str,
    experiment_label: str,
    screen_width: int,
    screen_height: int,
    measurement_duration: int,
) -> None:
    with open(url_file, encoding='utf-8') as f:
        for line in f.readlines():
            url = line.rstrip()
            print(f"Will measure consumption of {url} on {adb_device} for {measurement_duration} s")

            subprocess.run(
                ['adb',
                    '-s', adb_device,
                    'shell', 'am', 'start',
                    '-a', 'android.intent.action.VIEW',
                    # This makes the browser reuse the current tab instead of
                    # opening a new one
                    '--es', '"com.android.browser.application_id"', '"com.android.chrome"',
                    '-d', url,
                ],
                check=True,
            )

            time.sleep(2)

            if browser == 'firefox':
                sys.exit("Firefox is not supported")
            else:
                bypass_chrome_https_warning(
                    adb_device,
                    screen_width,
                    screen_height,
                    browser,
                )

            subprocess.run(
                ['adb', '-s', adb_device, 'shell', 'dumpsys', 'batterystats', '--reset'],
                check=True,
            )

            print(f"Waiting for {measurement_duration} s…")
            time.sleep(measurement_duration)

            stats = subprocess.run(
                ['adb', '-s', adb_device, 'shell', 'dumpsys', 'batterystats'],
                check=True,
                capture_output=True,
                encoding='utf-8',
            )
            timestamp = int(time.time())
            output_file = f'data/batterystats_{experiment_label}_{device_name}_{normalize_url(url)}_{timestamp}.txt'
            with open(output_file, 'w', encoding='utf-8') as of:
                of.write(stats.stdout)
            print(f"Saved batterystats to {output_file}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("url_file", type=pathlib.Path, metavar="INPUT",
                        help="input URL file")
    parser.add_argument("--adb-device", type=str, required=True,
                        dest="adb_device", help="ADB device")
    parser.add_argument("--device-name", type=str, required=True,
                        dest="device_name", help="device name")
    parser.add_argument("--browser", type=str, required=True, dest="browser",
                        help="browser ('firefox', 'chrome', or 'oldchrome')")
    parser.add_argument("--experiment-label", type=str, required=True,
                        dest="experiment_label", help="experiment label")
    parser.add_argument("--screen-width", type=int, required=True,
                        dest="screen_width", help="screen width, in pixels")
    parser.add_argument("--screen-height", type=int, required=True,
                        dest="screen_height", help="screen height, in pixels")
    parser.add_argument("--measurement-duration", type=int, required=True,
                        dest="measurement_duration",
                        help="measurement duration, in seconds")
    args = parser.parse_args()

    measure_power_consumption(
        args.url_file,
        args.adb_device,
        args.device_name,
        args.browser,
        args.experiment_label,
        args.screen_width,
        args.screen_height,
        args.measurement_duration,
    )
